<?php

/**
 * Element Controls: Owl Carousel
 */

return array(

	'item' => array(
		'type'	=> 'sortable',
		'key'	=> 'elements',
		'ui'	=> array(
			'title'		=> __('Items', 'rp-owl-textdomain'),
			'tooltip'	=> __('Add carousel items', 'rp-owl-textdomain'),
		),
		'options' => array(
			'element'	=> 'owl-item',
			'floor'		=> 1,
			'divider'	=> true,
		),
	),

	'items' => array(
		'type' 	=> 'text',
		'ui'	=> array(
			'title'		=> __('Amount of items', 'rp-owl-textdomain'),
			'tooltip'	=> __('How many items should we show initially', 'rp-owl-textdomain'),
		),
		'context' => 'content',
		'suggested' => '3',
	),

	'margin' => array(
		'type' 	=> 'text',
		'ui'	=> array(
			'title'		=> __('Margin', 'rp-owl-textdomain'),
			'tooltip'	=> __('Add margin in between items', 'rp-owl-textdomain'),
		),
		'context' 	=> 'content',
		'suggest'	=> '10',
	),

	'center' => array(
		'type'	=> 'toggle',
		'ui'	=> array(
			'title' 	=> __('Center', 'rp-owl-textdomain'),
			'tooltip'	=> __('Center the carousel. This is used for uneven numbered carousels above 5', 'rp-owl-textdomain'),
		),
	),

	'loop' => array(
		'type'	=> 'toggle',
		'ui'	=> array(
			'title' 	=> __('Loop', 'rp-owl-textdomain'),
			'tooltip'	=> __('Append the first carousel at the end to make it look like it loops around', 'rp-owl-textdomain'),
		),
	),

	'dots' => array(
		'type'	=> 'toggle',
		'ui'	=> array(
			'title' 	=> __('Pagination', 'rp-owl-textdomain'),
			'tooltip'	=> __('Enable the bullet navigation', 'rp-owl-textdomain'),
		),
	),

	'nav' => array(
		'type'	=> 'toggle',
		'ui'	=> array(
			'title' 	=> __('Navigation', 'rp-owl-textdomain'),
			'tooltip'	=> __('Enable arrow navigation', 'rp-owl-textdomain'),
		),
	),

	'lazyload' => array(
		'type'	=> 'toggle',
		'ui'	=> array(
			'title' 	=> __('LazyLoad', 'rp-owl-textdomain'),
			'tooltip'	=> __('Allow the carousel to lazyload the images and content', 'rp-owl-textdomain'),
		),
	),

	'autoplay' => array(
		'type'	=> 'toggle',
		'ui'	=> array(
			'title' 	=> __('Autoplay', 'rp-owl-textdomain'),
			'tooltip'	=> __('Allow the carousel to slide automatically', 'rp-owl-textdomain'),
		),
	),

	'autoplaytimeout' => array(
		'type'	=> 'text',
		'ui'	=> array(
			'title' 	=> __('Autoplay Speed', 'rp-owl-textdomain'),
			'tooltip'	=> __('Set the amount of wait time between slide in miliseconds', 'rp-owl-textdomain'),
		),
		'context' => 'content',
		'suggets' => '5000',
	),

	'pauseonhover' => array(
		'type'	=> 'toggle',
		'ui'	=> array(
			'title' 	=> __('Pause on hover', 'rp-owl-textdomain'),
			'tooltip'	=> __('When hovering over a item with the mouse the carousel will pause', 'rp-owl-textdomain'),
		),
	),

	'animatein' => array(
		'type'	=> 'choose',
		'ui'	=> array(
			'title' 	=> __('Center', 'rp-owl-textdomain'),
			'tooltip'	=> __('Center the carousel. This is used for uneven numbered carousels above 5', 'rp-owl-textdomain'),
		),
		'options' => array(
			'columns' => '2',
			'choices' => array(
          		array( 'value' => 'default',  		'label' => __( 'Default', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'slideInDown', 	'label' => __( 'slideInDown', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'slideInLeft', 	'label' => __( 'slideInLeft', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'slideInRight', 	'label' => __( 'slideInRight', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'slideInUp', 		'label' => __( 'slideInUp', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'bounceInDown', 	'label' => __( 'bounceInDown', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'bounceInLeft', 	'label' => __( 'bounceInLeft', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'bounceInRight', 	'label' => __( 'bounceInRight', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'bounceInUp', 	'label' => __( 'bounceInUp', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'flipInX', 		'label' => __( 'flipInX', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'flipInY', 		'label' => __( 'flipInY', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'zoomIn', 		'label' => __( 'zoomIn', 'rp-owl-textdomain' ) ),
	        )
		),
	),

	'animateout' => array(
		'type'	=> 'choose',
		'ui'	=> array(
			'title' 	=> __('Center', 'rp-owl-textdomain'),
			'tooltip'	=> __('Center the carousel. This is used for uneven numbered carousels above 5', 'rp-owl-textdomain'),
		),
		'options' => array(
			'columns' => '2',
			'choices' => array(
          		array( 'value' => 'default',  		'label' => __( 'Default', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'slideOutLeft', 	'label' => __( 'slideOutLeft', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'slideOutRight', 	'label' => __( 'slideOutRight', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'slideOutUp', 	'label' => __( 'slideOutUp', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'bounceOutDown', 	'label' => __( 'bounceOutDown', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'bounceOutLeft',	'label' => __( 'bounceOutLeft', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'bounceOutRight', 'label' => __( 'bounceOutRight', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'bounceOutUp', 	'label' => __( 'bounceOutUp', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'flipOutX', 		'label' => __( 'flipOutX', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'flipOutY', 		'label' => __( 'flipOutY', 'rp-owl-textdomain' ) ),
          		array( 'value' => 'zoomOut', 		'label' => __( 'zoomOut', 'rp-owl-textdomain' ) ),
	        )
		),
	),
);
