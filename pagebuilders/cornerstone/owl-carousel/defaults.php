<?php

/**
 * Element Defaults: Owl Carousel
 */

return array(
	'id'				=> '',
	'class'				=> '',
	'style'				=> '',
	'items'				=> '3',
	'margin'			=> '0',
	'center'			=> false,
	'loop'				=> false,
	'dots'				=> false,
	'nav'				=> false,
	'lazyload'			=> false,
	'autoplay'			=> false,
	'autoplaytimeout'	=> '5000',
	'pauseonhover'		=> false,
	'animatein'			=> 'default',
	'animateout'		=> 'default',
);
