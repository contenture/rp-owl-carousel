<?php

/**
 * Element Shortcode: Owl Carousel
 */

$random = rand();
$id = trim("owl-carousel-" . $random);
$class = trim("owl-carousel" . ' ' . $class);

?>

<div <?php cs_atts(array('id' => $id, 'class' => $class, 'style' => $style)); ?>>
	<?php echo do_shortcode($content); ?>
</div>

<script type="text/javascript">
	jQuery("#<?php $id; ?>").owlCarousel({
		items: <?php echo $items; ?>,
		margin: <?php echo $margin; ?>,
		center: <?php echo $center; ?>,
		loop: <?php echo $loop; ?>,
		dots: <?php echo $dots; ?>,
		nav: <?php echo $nav; ?>,
		lazyLoad: <?php echo $lazyload; ?>,
		autoplay: <?php echo $autoplay; ?>,
		autoplayTimeout: <?php echo $autoplaytimeout; ?>,
		autoplayHoverPause: <?php echo $pauseonhover; ?>,
		animateIn: <?php echo $animatein; ?>,
		animateOut: <?php echo $animateout; ?>,
	});
</script>
