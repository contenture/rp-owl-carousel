<?php

/**
 * Element Definition: Owl Carousel
 */

class RPCSE_Owl_Carousel {

	public function ui() {
		return array(
			'title'		=> __("Owl Carousel", 'rp-owl-textdomain'),
			'icon'		=> 'assets/owl-carousel.svg',
			'autofocus'	=> array(
				'content' => '.owl-carousel',
			)
		);
	}

}
