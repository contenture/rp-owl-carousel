<?php

/**
 * Element Defaults: Owl Item
 */

return array(
	'id'		=> '',
	'class'		=> '',
	'style'		=> '',
	'lazyload'	=> false,
	'image'		=> 'http://placehold.it/200x100/',
	'is_link'	=> false,
	'url'		=> '',
	'target'	=> false,
);
