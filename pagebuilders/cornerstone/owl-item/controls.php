<?php

/**
 * Element Controls: Owl Item
 */

return array(

	'lazyload' => array(
		'type'	=> 'toggle',
		'ui'	=> array(
			'title'		=> __('Lazyload', 'rp-owl-textdomain'),
			'tooltip'	=> __('Do you want to lazyload the items', 'rp-owl-textdomain'),
		),
	),

	'image' => array(
		'type'	=> 'image',
		'ui'	=> array(
			'title'		=> __('Image', 'rp-owl-textdomain'),
			'tooltip'	=> __('Upload the image of the item', 'rp-owl-textdomain'),
		),
		'options'	=> array(
			'placeholder'	=> 'http://placehold.it/200x100/',
		),
	),

	'is_link' => array(
		'type'	=> 'toggle',
		'ui'	=> array(
			'title' 	=> __('Link', 'rp-owl-textdomain'),
			'tooltip'	=> __('Is the item clickable', 'rp-owl-textdomain'),
		),
	),

	'url' => array(
		'type' 	=> 'text',
		'ui'	=> array(
			'title'		=> __('Url', 'rp-owl-textdomain'),
			'tooltip'	=> __('The Url to link to', 'rp-owl-textdomain'),
		),
		'context'	=> 'content',
		'suggest'	=> '#',
	),

	'target' => array(
		'type'	=> 'toggle',
		'ui'	=> array(
			'title'		=> __('Open in new tab?', 'rp-owl-textdomain'),
			'tooltip'	=> __('Do you want the item to open in a new tab/window', 'rp-owl-textdomain'),
		),
	),

);
