<?php

/**
 * Element Definition: Owl Item
 */

class RPCSE_Owl_Item {

	public function ui() {
		return array(
			'title'		=> __('Owl Item', 'rp-owl-textdomain'),
		);
	}

	public function flags() {
		return array(
			'context'	=> 'all',
			'child'		=> true,
		);
	}

}
