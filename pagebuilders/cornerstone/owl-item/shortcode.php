<?php

/**
 * Element Shortcode: Owl Item
 */

$isLazyLoad 	= ($lazyload) ? 'class="owl-lazy"' : '';
$target_link	= ($target) ? 'target="_blank"' : 'target="_self"';

$class = trim("item " . $class);

?>

<div <?php cs_atts(array('id' => $id, 'class' => $class, 'style' => $style)); ?>>
	<?php if ($is_link) : ?>
		<a href="<?php echo $url; ?>" <?php $target_link; ?>><img <?php $isLazyLoad; ?> src="<?php echo $image; ?>"/>
			<?php echo do_shortcode($content); ?>
		</a>
	<?php endif; ?>
</div>
