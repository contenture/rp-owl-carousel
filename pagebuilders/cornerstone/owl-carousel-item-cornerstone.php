<?php

class Owl_Carousel_Item extends Cornerstone_Element_Base {

  	public function data() {
    	return array(
			'name'        => 'owl-item',
			'title'       => __('Item', 'rp-owl-textdomain'),
			'section'     => '_content',
			'description' => __('Item description.', 'rp-owl-textdomain'),
			'supports'    => array('id', 'class', 'style'),
			'render'      => false,
			'delegate'    => true,
    	);
  	}

  public function controls() {

    	$this->addControl(
      		'title',
      		'title',
      		NULL,
      		NULL,
      		''
    	);

    	$this->addControl(
			'src',
			'image',
			__('Src', 'rp-owl-textdomain'),
			__('Enter your image.', 'rp-owl-textdomain'),
			'http://placehold.it/1200x600/3498db/2980b9'
    	);

    	$this->addControl(
	        'islink',
	        'toggle',
	        __('Make link?', 'rp-owl-textdomain'),
	        __('Make the item a link', 'rp-owl-textdomain'),
	        false
    	);

    	$this->addSupport('link');

 	}

  	public function xsg() {
  		$this->sg_map(
  			array(
  				'id'			=> 'x_carousel_item',
  				'title'			=> __('Item', 'rp-owl-textdomain'),
  				'weight'		=> 600,
  				'icon'			=> 'slide',
  				'section'		=> __('Media', 'rp-owl-textdomain'),
  				'description'	=> __('Include a item into your carousel', 'rp-owl-textdomain'),
  				'params'		=> array(
  					array(
	  					'param_name'	=> 'content',
	  					'heading'		=> __('Item', 'rp-owl-textdomain'),
	  					'description'	=> __('Add your item', 'rp-owl-textdomain'),
	  					'type'			=> 'textarea_html',
	  					'value'			=> ''
	  				),
  				),
  				Cornerstone_Shortcode_Generator::map_default_id();
  				Cornerstone_Shortcode_Generator::map_default_class();
  				Cornerstone_Shortcode_Generator::map_default_style();
  			)
  		);
  	}
}
?>
