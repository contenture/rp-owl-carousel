<?php

class Owl_Carousel extends Cornerstone_Element_Base {

	public function data() {
		return array(
			'name'			=> 'owl-carousel',
			'title'			=> __('Owl Carousel', 'rp-owl-textdomain'),
			'section'		=> 'media',
			'description'	=> __('Adds a Owl Carousel Element', 'rp-owl-textdomain'),
			'supports'		=> array('id', 'class', 'style'),
			// 'childType'		=> 'owl-item',
			'renderChild'	=> true,
		);
	}

	public function controls() {

		$this->addControl(
			'elements',
			'sortable',
			__( 'Items', 'rp-owl-textdomain' ),
			__( 'Add a new item to your carousel.', 'rp-owl-textdomain' ),
			array(
				array( 'title' => __( 'Item 1', 'rp-owl-textdomain' ), 'src' => 'http://placehold.it/1200x600/3498db/2980b9', 'islink' => false ),
				array( 'title' => __( 'Item 2', 'rp-owl-textdomain' ), 'src' => 'http://placehold.it/1200x600/3498db/2980b9', 'islink' => false ),
				array( 'title' => __( 'Item 3', 'rp-owl-textdomain' ), 'src' => 'http://placehold.it/1200x600/3498db/2980b9', 'islink' => false )
			),
			array(
				'element'	=> 'owl-item',
				'newTitle'	=> __( 'Slide %s', 'rp-owl-textdomain' ),
				'floor'   	=> 1
			)
		);

		$this->addControl(
	    	'items',
	      	'number',
	      	__( 'Items', 'rp-owl-textdomain' ),
	      	__( 'The amount of slides on one carousel.', 'rp-owl-textdomain' ),
	      	'3'
	    );

	    $this->addControl(
	    	'margin_size',
	      	'number',
	      	__( 'Margin', 'rp-owl-textdomain' ),
	      	__( 'Enter in the size of your margin in pixels.', 'rp-owl-textdomain' ),
	      	'0'
	    );

	    $this->addControl(
      		'loop',
      		'toggle',
      		__( 'Loop', 'rp-owl-textdomain' ),
      		__( 'Loop the carousel infinitely', 'rp-owl-textdomain' ),
      		false
    	);

    	$this->addControl(
      		'center',
      		'toggle',
      		__( 'Center', 'rp-owl-textdomain' ),
      		__( 'Center the items (works great with uneven numbers)', 'rp-owl-textdomain' ),
      		false
    	);

		$this->addControl(
      		'navigation',
      		'toggle',
      		__( 'Navigation', 'rp-owl-textdomain' ),
      		__( 'Show Prev/Next buttons', 'rp-owl-textdomain' ),
      		false
    	);

		$this->addControl(
      		'lazyload',
      		'toggle',
      		__( 'LazyLoad', 'rp-owl-textdomain' ),
      		__( 'Enabling this control will have your carousel lazy load images.', 'rp-owl-textdomain' ),
      		false
    	);

    	$this->addControl(
      		'pagination',
      		'toggle',
      		__( 'Pagination', 'rp-owl-textdomain' ),
      		__( 'Show the bullets under the carousel', 'rp-owl-textdomain' ),
      		false
    	);

    	$this->addControl(
      		'autoplay',
      		'toggle',
      		__( 'Autplay', 'rp-owl-textdomain' ),
      		__( 'Move through the carousel automatically', 'rp-owl-textdomain' ),
      		false
    	);

    	$this->addControl(
	    	'autoplayspeed',
	      	'number',
	      	__( 'Autoplay speed', 'rp-owl-textdomain' ),
	      	__( 'Enter in the speed of your autoplay in miliseconds.', 'rp-owl-textdomain' ),
	      	'5000'
	    );

    	$this->addControl(
      		'pauseonhover',
      		'toggle',
      		__( 'Pause on hover', 'rp-owl-textdomain' ),
      		__( 'Pause the autoplay when you hover over the carousel', 'rp-owl-textdomain' ),
      		false
    	);

    	$this->addControl(
	    	'animate_in',
	      	'select',
	      	__( 'Animation in', 'rp-owl-textdomain' ),
	      	__( 'Choose a animation to use for the enter animation', 'rp-owl-textdomain' ),
	      	'default',
	      	array(
	        	'columns' => '2',
	        	'choices' => array(
	          		array( 'value' => 'default',  		'label' => __( 'Default', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'slideInDown', 	'label' => __( 'slideInDown', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'slideInLeft', 	'label' => __( 'slideInLeft', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'slideInRight', 	'label' => __( 'slideInRight', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'slideInUp', 		'label' => __( 'slideInUp', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'bounceInDown', 	'label' => __( 'bounceInDown', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'bounceInLeft', 	'label' => __( 'bounceInLeft', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'bounceInRight', 	'label' => __( 'bounceInRight', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'bounceInUp', 	'label' => __( 'bounceInUp', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'flipInX', 		'label' => __( 'flipInX', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'flipInY', 		'label' => __( 'flipInY', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'zoomIn', 		'label' => __( 'zoomIn', 'rp-owl-textdomain' ) ),
	        	)
	      	)
	    );

	    $this->addControl(
	    	'animate_out',
	      	'select',
	      	__( 'Animation out', 'rp-owl-textdomain' ),
	      	__( 'Choose a animation to use for the exit animation', 'rp-owl-textdomain' ),
	      	'default',
	      	array(
	        	'columns' => '2',
	        	'choices' => array(
	          		array( 'value' => 'default',  		'label' => __( 'Default', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'slideOutLeft', 	'label' => __( 'slideOutLeft', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'slideOutRight', 	'label' => __( 'slideOutRight', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'slideOutUp', 	'label' => __( 'slideOutUp', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'bounceOutDown', 	'label' => __( 'bounceOutDown', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'bounceOutLeft',	'label' => __( 'bounceOutLeft', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'bounceOutRight', 'label' => __( 'bounceOutRight', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'bounceOutUp', 	'label' => __( 'bounceOutUp', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'flipOutX', 		'label' => __( 'flipOutX', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'flipOutY', 		'label' => __( 'flipOutY', 'rp-owl-textdomain' ) ),
	          		array( 'value' => 'zoomOut', 		'label' => __( 'zoomOut', 'rp-owl-textdomain' ) ),
	        	)
	      	)
	    );
	}

	public function xsg() {
		$this->sg_map(
			array(
				'id'			=> 'x_carousel',
		    	'title'			=> __( 'Owl Carouse', 'rp-owl-textdomain' ),
		    	'weight'		=> 590,
		    	'icon'			=> 'slider',
		    	'section'		=> __( 'Media', 'rp-owl-textdomain' ),
		    	'description'	=> __( 'Include a carousel in your content', 'rp-owl-textdomain' ),
		    	'params'		=> array(
		    		array(
		    			'param_name'	=> 'items',
		    			'heading'		=> __('Number of Items', 'rp-owl-textdomain'),
		    			'description'	=> __('The amount of Items you want to show', 'rp-owl-textdomain'),
		    			'type'			=> 'textfield',
		    			'value'			=> '3'
		    		),
		    	),
		    	Cornerstone_Shortcode_Generator::map_default_id(),
		      	Cornerstone_Shortcode_Generator::map_default_class(),
		      	Cornerstone_Shortcode_Generator::map_default_style()
			)
		);
	}

	public function render($atts) {
		extract($atts);

		$content = '';

		foreach ($elements as $e) {

      		$item_extra = $this->extra( array(
        		'id'    => $e['id'],
        		'class' => $e['class'],
        		'style' => $e['style']
      		));

      		$contents .= "[x_carousel_item" . $item_extra . " lazyload=\"$lazyload\" video=\"$enablevideo\" image='" . $e['src'] . "' is_link='" . $e['islink'] . "' url='" . $e['href'] . "' target='" . $e['href_target'] . "']" . $e['content'] . "[/x_carousel_item]";
    	}

    	$shortcode = "[x_carousel items=\"$items\" margin=\"$margin_size\" center=\"$center\" loop=\"$loop\" pagination=\"$pagination\" lazyload=\"$lazyload\" navigation=\"$navigation\" autoplay=\"$autoplay\" autoplayspeed=\"$autoplayspeed\" pauseonhover=\"$pauseonhover\" video=\"$enablevideo\" animatein=\"$animate_in\" animateout=\"$animate_out\"]{$contents}[/x_carousel]";

		return $shortcode;
	}
}

?>
