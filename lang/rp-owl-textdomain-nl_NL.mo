��    $      <  5   \      0      1     R     n     {     �     �     �  2   �  1   �  0        =  ?   E  +   �  3   �     �     �     �          
          #     )  
   F     Q     f  '   m  
   �  
   �     �  3   �     �  #        )     2  %   6  L  \  (   �  #   �     �     	     	      	     5	  E   >	  9   �	  :   �	  	   �	  &   
  -   *
  7   X
     �
     �
     �
     �
     �
     �
     �
      �
     �
            &   #  	   J  
   T     _  B   v     �     �     �     �  +                                      #   "                     	                  !       
                                                                   $                             Add a new item to your carousel. Adds a Owl Carousel Element Animation in Animation out Autoplay speed Autplay Center Center the items (works great with uneven numbers) Choose a animation to use for the enter animation Choose a animation to use for the exit animation Default Enabling this control will have your carousel lazy load images. Enter in the size of your margin in pixels. Enter in the speed of your autoplay in miliseconds. Enter your image. Item Item 1 Item 2 Item 3 Item description. Items Loop the carousel infinitely Make link? Make the item a link Margin Move through the carousel automatically Navigation Pagination Pause on hover Pause the autoplay when you hover over the carousel Show Prev/Next buttons Show the bullets under the carousel Slide %s Src The amount of slides on one carousel. Project-Id-Version: RP Owl Carousel
POT-Creation-Date: 2015-11-30 23:34+0100
PO-Revision-Date: 2015-11-30 23:42+0100
Last-Translator: 
Language-Team: RedactiePartners <it@redactiepartners.nl>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.6
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: rp-owl-carousel.php
X-Poedit-SearchPath-1: pagebuilders/cornerstone/owl-carousel-cornerstone.php
X-Poedit-SearchPath-2: pagebuilders/cornerstone/owl-carousel-item-cornerstone.php
 Voeg een nieuw item toe aan je carousel. Voegt een Owl Carousel Element toe. Animatie in Animatie uit Afspeel snelheid Automatisch afspelen Centreer Centreer de items (dit is handig bij een carousel met oneven nummers) Kies het soort animatie voor het in animeren van de items Kies het soort animatie voor het uit animeren van de items Standaard Gebruik LazyLoad om de items te laden. Vul hier de grote van je marge toe in pixels. Geef de wachttijd van het afspelen aan in milliseconde. Voeg je afbeelding toe Item Item #1 Item #2 Item #3 Item omschrijving Items Loop de carrousel oneindig door. Klikbaar Maak het item klikbaar Marge Laat de carrousel automatisch afspelen Navigatie Pagina’s Pauzeer bij muis over. Pauzeer het afspelen wanneer je met je muis over de carousel gaat. Toon vorige/volgende knoppen. Toon bullets onder de carousel. Item %s Afbeelding bron Het aantal items wat je op de pagina toont. 