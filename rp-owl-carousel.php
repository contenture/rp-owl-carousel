<?php
/*
	Plugin Name: RP Owl Carousel
	Description: Adds Owl Carousel Functionality to X Theme, Cornerstone
	Author: Patrick van Zadel
	Version: 1.0.1
	Author URI: http://www.redactiepartners.nl
	Licence: GPL2
*/

// Include Plugin PHP for is_plugin_active function
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

add_action('plugins_loaded', 'rp_owl_carousel_load_textdomain'); // Load the text languages
add_action('init', 'rp_carousel_init'); // Initialize the plugin at the correct time
add_action('wp_print_scripts', 'rp_register_scripts'); // Register the JS files
add_action('wp_print_styles', 'rp_register_styles'); // Register the CSS Files

function rp_carousel_init() {
	// Adds the actual shortcodes
	add_shortcode('cs_owl_carousel', 'rp_carousel_function');
	add_shortcode('cs_owl_item', 'rp_carousel_item_function');
	// Adds Shortcode for carousel post loop
	add_shortcode('cs_owl_carousel_post', 'rp_carousel_post_function');

  	// Add Cornerstone Element
    add_action('cornerstone_load_elements', 'rp_owl_carousel');
}

/**
 * List of JavaScript files
 */
function rp_register_scripts() {
    wp_register_script('js.owl.carousel', plugins_url('owl-carousel/owl.carousel.min.js', __FILE__));
    wp_enqueue_script('jquery');
    wp_enqueue_script('js.owl.carousel');
}

/**
 * List of CSS files
 */
function rp_register_styles() {
	wp_register_style('style.owl.carousel', plugins_url('owl-carousel/assets/owl.carousel.css', __FILE__));
    wp_register_style('style.owl.carousel.theme', plugins_url('owl-carousel/assets/owl.carousel.theme.default.css', __FILE__));
    wp_register_style('style.owl.carousel.animate', plugins_url('owl-carousel/assets/animate.css', __FILE__));
    wp_register_style('style.owl.carousel.main', plugins_url('owl-carousel/assets/main.css', __FILE__));
    wp_enqueue_style('style.owl.carousel');
    wp_enqueue_style('style.owl.carousel.theme');
    wp_enqueue_style('style.owl.carousel.animate');
    wp_enqueue_style('style.owl.carousel.main');
}

/**
 * Load the plugin languages
 */
function rp_owl_carousel_load_textdomain() {
	load_plugin_textdomain('rp-owl-textdomain', false, plugins_url('/lang/', __FILE__));
}

/**
* Shortcode carousel item function
*
**/
function rp_carousel_function($atts, $content = null) {
	extract(shortcode_atts(array(
        'navigation'     => false,
        'loop'           => false,
      	'lazyload'       => false,
      	'pagination'     => false,
      	'items'          => '3',
      	'margin'         => '0',
        'center'         => false,
      	'autoplay'       => false,
      	'autoplayspeed'  => '5000',
      	'pauseonhover'   => false,
        'video'          => false,
        'animatein'      => 'default',
        'animateout'     => 'default',
    ), $atts));

    $isloop           	= ( $loop 			== 'true') ? 'true' : 'false';
    $iscenter         	= ( $center 		== 'true') ? 'true' : 'false';
    $nav              	= ( $navigation  	== 'true') ? 'true' : 'false';
    $enablelazy       	= ( $lazyload  		== 'true') ? 'true' : 'false';
    $enablepagination 	= ( $pagination 	== 'true') ? 'true' : 'false';
    $isautoplay       	= ( $autoplay 		== 'true') ? 'true' : 'false';
    $dopauseonhover   	= ( $pauseonhover 	== 'true') ? 'true' : 'false';
    $enablevideo      	= ( $video 			== 'true') ? 'true' : 'false';
    $animateIn        	= ( $animatein 		== 'default') ? 'false' : '"' . $animatein . '"';
    $animateOut       	= ( $animateout 	== 'default') ? 'false' : '"' . $animateout . '"';

    $random     		= rand();
    $data_attr  		= "data-lazyload=\"$enablelazy\"";
    $scripttag  		= "";

    $result   			= '<div id="owl-carousel-' . $random . '" class="owl-carousel">';
    $result   			.= do_shortcode($content);
    $result   			.= '</div>';

    $scripttag 			.= '<script type="text/javascript">';
    $scripttag 			.= 'jQuery("#owl-carousel-' . $random .'").owlCarousel({';
    $scripttag 			.= 'items: ' . $items .', margin: ' . $margin . ', center: ' . $iscenter . ', loop: ' . $isloop . ', dots: ' . $enablepagination . ', nav: ' . $nav . ', lazyLoad: ' . $enablelazy . ', autoplay: ' . $isautoplay . ', autoplayTimeout: ' . $autoplayspeed . ', autoplayHoverPause: ' . $dopauseonhover . ', animateIn: ' . $animateIn . ', animateOut: ' . $animateOut . ', video: ' . $enablevideo . '});';
    $scripttag 			.= '</script>';

    return $result . $scripttag;
}

/**
* Shortcode carousel item function
*
**/
function rp_carousel_item_function($atts, $content = null) {
	extract(shortcode_atts(array(
		'lazyload' 	=> false,
    	'video' 	=> false,
		'image' 	=> '',
    	'is_link' 	=> false,
    	'url' 		=> '',
    	'target' 	=> false,
	), $atts));

	$isLazyload 		= ( $lazyload 	== 'true') ? 'true' : 'false';
	$isvideo 			= ( $video 		== 'true') ? 'true' : 'false';
	$islink 			= ( $is_link 	== 'true') ? 'true' : 'false';
	$url 				= ( $url 		!= '') ? $url : '#';
	$istarget 			= ( $target 	== 'true') ? '_blank' : '_self';

  	$itemresult = '<div class="item">';
  	if ($isLazyload == 'true') {
  		if ($isvideo == 'true') {
      		$itemresult .= '<div class="item-video"><a class="owl-video" href="' . $image . '"></a></div>';
    	} else {
      		$itemresult .= '<img class="owl-lazy" data-src="' . $image . '" />';
    	}
  	} else {
    	if ($isvideo == 'true') {
      		$itemresult .= '<div class="item-video"><a class="owl-video" href="' . $image . '"></a></div>';
    	} else {
      		if ($islink == 'true') {
        		$itemresult .= '<a href="' . $url . '" target="' . $istarget . '"><img src="' . $image . '" /></a>';
      		} else {
        		$itemresult .= '<img src="' . $image . '" />';
      		}
    	}
  	}
	$itemresult .= '</div>';

	return $itemresult;
}

/**
* Shortcode carousel Post Query
*
**/
function rp_carousel_post_function($atts, $content = null) {
	extract(shortcode_atts(array(
		'category'		 => '',
		'navigation'     => false,
        'loop'           => false,
      	'lazyload'       => false,
      	'pagination'     => false,
      	'items'          => '1',
      	'margin'         => '0',
        'center'         => false,
      	'autoplay'       => false,
      	'autoplayspeed'  => '5000',
      	'pauseonhover'   => false,
        'video'          => false,
        'animatein'      => 'default',
        'animateout'     => 'default',
	), $atts));

	$isloop           	= ( $loop 			== 'true') ? 'true' : 'false';
    $iscenter         	= ( $center 		== 'true') ? 'true' : 'false';
    $nav              	= ( $navigation  	== 'true') ? 'true' : 'false';
    $enablelazy       	= ( $lazyload  		== 'true') ? 'true' : 'false';
    $enablepagination 	= ( $pagination 	== 'true') ? 'true' : 'false';
    $isautoplay       	= ( $autoplay 		== 'true') ? 'true' : 'false';
    $dopauseonhover   	= ( $pauseonhover 	== 'true') ? 'true' : 'false';
    $enablevideo      	= ( $video 			== 'true') ? 'true' : 'false';
    $animateIn        	= ( $animatein 		== 'default') ? 'false' : '"' . $animatein . '"';
    $animateOut       	= ( $animateout 	== 'default') ? 'false' : '"' . $animateout . '"';

    $random     		= rand();
    $data_attr  		= "data-lazyload=\"$enablelazy\"";
    $scripttag  		= "";

	// WP_Query arguments
	$args = array (
		'post_status'	=> array( 'publish' ),
		'cat'			=> $category,
		'post_per_page'	=> '2',
	);

	$result   			= '<div id="owl-carousel-' . $random . '" class="owl-carousel">';

	// The Query
	$query = new WP_Query( $args );

	// The Loop
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();

				$img_src = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), get_post_type());

				if ($img_src[0]) {
					$result .= '<div class="item">';
						$result .= '<a href="' . get_permalink() . '" target="_self">';
							if ($enablelazy == 'true') {
								$result .= '<img class="owl-lazy" title="' . get_the_title() . '" data-src="' . $img_src[0] . '" alt="' . get_the_title() . '" />';
							} else {
								$result .= '<img title="' . get_the_title() . '" src="' . $img_src[0] . '" alt="' . get_the_title() . '" />';
							}
							$result .= '<div class="owl-content">';
								$result .= '<h2 class="owl-title">' . get_the_title() . '</h2>';
								$result .= '<p class="owl-excerpt">' . get_the_excerpt() . '</p>';
							$result .= '</div>';
						$result .= '</a>';
					$result .= '</div>';
				}
		}
	} else {
		// no posts found
	}

	// Restore original Post Data
	wp_reset_postdata();

	$result   			.= '</div>';

    $scripttag 			.= '<script type="text/javascript">';
    $scripttag 			.= 'jQuery("#owl-carousel-' . $random .'").owlCarousel({';
    $scripttag 			.= 'items: ' . $items .', margin: ' . $margin . ', center: ' . $iscenter . ', loop: ' . $isloop . ', dots: ' . $enablepagination . ', nav: ' . $nav . ', lazyLoad: ' . $enablelazy . ', autoplay: ' . $isautoplay . ', autoplayTimeout: ' . $autoplayspeed . ', autoplayHoverPause: ' . $dopauseonhover . ', animateIn: ' . $animateIn . ', animateOut: ' . $animateOut . ', video: ' . $enablevideo . '});';
    $scripttag 			.= '</script>';

    return $result . $scripttag;
}

/**
 * Load Cornerstone Element if needed
 *
 **/
function rp_owl_carousel() {
  	$owlitempath = dirname(__FILE__) . '/pagebuilders/cornerstone/owl-item';
	$owlcarouselpath = dirname(__FILE__) . '/pagebuilders/cornerstone/owl-carousel';

  	cornerstone_register_element('RPCSE_Owl_Item', 'owl-item', $owlitempath);
  	cornerstone_register_element('RPCSE_Owl_Carousel', 'owl_carousel', $owlcarouselpath);
}

?>
