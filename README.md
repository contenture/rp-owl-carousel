RedactiePartners Owl Carousel
=============================

This plugin provides [Owl Carousel 2](http://www.owlcarousel.owlgraphic.com/) functionality.

By utilizing multiple API's from Visual Page Builder Plugins we hope to provide an easy and simple solution for adding a Owl Carousel in the best way possible.

This is currently in development and not really usable.

We strive to provide elements for the following page builders:

1. Cornerstone [By theme.co](https://theme.co/cornerstone/)
2. Visual Composer [By WPBakery](http://vc.wpbakery.com/)

License
=======
Copyright (c) <year> <copyright holders>



Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:



The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.



THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
